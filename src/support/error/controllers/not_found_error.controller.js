(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.support.error')
    .controller('NotFoundErrorController', NotFoundErrorController);

  /** @ngInject */
  function NotFoundErrorController () {
    var vm = this;

    (function () {
      vm.message = 'Ops...Resource not found!!!';
    })();
  }
})();
