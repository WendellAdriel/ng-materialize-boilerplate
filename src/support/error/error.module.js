(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.support.error', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig ($stateProvider) {
    $stateProvider
      .state('not_found', {
        url         : '/404',
        templateUrl : 'support/error/views/404.html',
        controller  : 'NotFoundErrorController as vm',
      });
  }
})();
