(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.support', [
      'ngMaterializeBoilerplate.support.error',
    ]);
})();
