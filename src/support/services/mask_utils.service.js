(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.support')
    .factory('MaskUtilsService', MaskUtilsService);

  /** @ngInject */
  function MaskUtilsService () {
    var services = {
      removeMask : removeMask,
    };

    return services;

    function removeMask (value) {
      var notMasked = value.replace(/\D/g,'');
      return notMasked;
    }
  }
})();
