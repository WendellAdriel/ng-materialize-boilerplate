(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.support')
    .factory('MessageService', MessageService);

  /** @ngInject */
  function MessageService (toastr) {
    var messageConfig = {
      autoDismiss           : true,
      positionClass         : 'toast-top-right',
      timeOut               : '3000',
      extendedTimeOut       : '2000',
      allowHtml             : false,
      closeButton           : true,
      tapToDismiss          : true,
      progressBar           : false,
      newestOnTop           : true,
      preventDuplicates     : false,
      preventOpenDuplicates : false,
    };

    var services = {
      info    : info,
      warning : warning,
      error   : error,
      success : success,
    };

    return services;

    function info (message) {
      toastr.info(message, messageConfig);
    }

    function warning (message) {
      toastr.warning(message, messageConfig);
    }

    function error (message) {
      toastr.error(message, messageConfig);
    }

    function success (message) {
      toastr.success(message, messageConfig);
    }
  }
})();
