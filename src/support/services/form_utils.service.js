(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.support')
    .factory('FormUtilsService', FormUtilsService);

  /** @ngInject */
  function FormUtilsService () {
    var services = {
      generalErrorMsg : 'There are errors in the form!',
      showFormErrors  : showFormErrors,
    };

    return services;

    function showFormErrors (errors) {
      errors.forEach(function (field) {
        field.forEach(function (errorField) {
          errorField.$setTouched();
        });
      });
    }
  }
})();
