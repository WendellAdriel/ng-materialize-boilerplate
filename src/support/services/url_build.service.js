(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.support')
    .factory('UrlBuildService', UrlBuildService);

  /** @ngInject */
  function UrlBuildService (config) {
    var services = {
      buildUrl : buildUrl,
    };

    return services;

    function buildUrl () {
      var urlArray = [config.apiHost];
      arguments.forEach(function (v, k) {
        urlArray.push(v);
      }, urlArray);
      var url = urlArray.join('/');
      return url;
    }
  }
})();
