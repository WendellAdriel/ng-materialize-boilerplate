(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.config')
    .factory('parseResponseInterceptor', parseResponseInterceptor);

  /** @ngInject */
  function parseResponseInterceptor () {
    return {
      response : function (response) {
        if (typeof response.data === 'object') {
          return response.data;
        }
        return response;
      }
    };
  }
})();
