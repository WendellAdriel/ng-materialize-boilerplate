(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.config')
    .factory('notFoundInterceptor', notFoundInterceptor);

  /** @ngInject */
  function notFoundInterceptor ($q, $injector) {
      return {
        responseError : function (rejection) {
          if (rejection.status === 404) {
            $injector.get('$state').go('not_found');
          }
          return $q.reject(rejection);
        }
      };
  }
})();
