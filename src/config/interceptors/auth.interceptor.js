(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.config')
    .factory('authInterceptor', authInterceptor);

  /** @ngInject */
  function authInterceptor ($q, $injector, config) {
    return {
      request : function (requestConfig) {
        requestConfig.headers = requestConfig.headers || {};
        var token = localStorage.getItem(config.token);
        if (token) {
          requestConfig.headers.Authorization = 'Bearer ' + token;
        }
        return requestConfig;
      },
      responseError : function (rejection) {
        if (rejection.status === 400 || rejection.status === 401 || rejection.status === 403) {
          $injector.get('$state').go('home');
        }
        return $q.reject(rejection);
      }
    };
  }
})();
