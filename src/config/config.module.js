(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.config', [])
    .config(config);

  /** @ngInject */
  function config ($httpProvider) {
    $httpProvider.interceptors.push('parseResponseInterceptor');
    $httpProvider.interceptors.push('notFoundInterceptor');
    $httpProvider.interceptors.push('authInterceptor');
  }
})();
