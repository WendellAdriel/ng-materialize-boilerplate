(function () {
  'use strict';

  var baseUrl    = 'YOUR_API_HOST'; // Example: 'http://myapi.com/'
  var apiVersion = 'YOUR_API_VERSION'; // Example: 'v1'

  angular
    .module('ngMaterializeBoilerplate.config')
    .constant('config', {
      apiHost : baseUrl + apiVersion,
      token   : 'YOUR_APP_TOKEN_NAME', // Example: 'myapp_token'
    });
})();
