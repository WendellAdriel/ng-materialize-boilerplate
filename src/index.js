(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate', [
      'ui.router',
      'ngAnimate',
      'ngTouch',
      'toastr',
      'ngMask',
      'ngMaterializeBoilerplate.config',
      'ngMaterializeBoilerplate.support',
      'ngMaterializeBoilerplate.app',
    ])
    .config(routesConfig);

  /** @ngInject */
  function routesConfig ($urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/404');
  }
})();
