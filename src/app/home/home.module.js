(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.app.home', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig ($stateProvider) {
    $stateProvider
      .state('home', {
        url         : '/',
        templateUrl : 'app/home/views/home.html',
        controller  : 'HomeController as vm',
      });
  }
})();
