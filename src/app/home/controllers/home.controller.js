(function () {
  'use strict';

  angular
    .module('ngMaterializeBoilerplate.app.home')
    .controller('HomeController', HomeController);

  /** @ngInject */
  function HomeController (HomeService) {
    var vm = this;

    (function () {
      vm.title = 'ngMaterializeBoilerplate Home Page';
    })();
  }
})();
