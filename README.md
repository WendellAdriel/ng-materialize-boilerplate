# NG Materialize Boilerplate

> Angular 1 boilerplate with Gulp, Materialize and other cools technologies using the best practices

## How to use

- Clone this repo;

- Install **NPM** dependencies:
```
npm i
```

- Install **Bower** dependencies:
```
bower install
```

## NPM Scripts

- Launch a browser sync server on source files:
```
npm run serve
```

- Build an optimized version of the application in `/dist`:
```
npm run build
```

- Launch a server on the optimized application:
```
npm run serve:dist
```

- Launch unit tests with **Karma**:
```
npm run test
```

- Launch unit tests with **Karma** in watch mode:
```
npm run test:auto
```
